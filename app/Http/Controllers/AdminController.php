<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use App\User;
use App\Price;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use View;
use Auth;
use Input;
use Hash;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$appointments = Appointment::orderBy('id', 'DESC')->get();

		return view('admin/index', compact('appointments'));
	}

	public function password()
	{
		$user = User::findOrFail(Auth::user()->id);

		return view('admin/password', compact('user'));
	}

	public function update($id, Request $request)
	{
		$user = User::findOrFail(Auth::user()->id);

		$user->name = Input::get('name');
		$user->email = Input::get('email');
		if(Input::has('password')) {
			$user->password = Hash::make(Input::get('password'));
		}
        $user->save();


		return redirect('/admin');
	}

	// Show prices
	public function prices()
	{
		$prices = Price::orderBy('days', 'DESC')->get();

		return view('admin/prices/index', compact('prices'));
	}

	// Create new price
	public function createprice(){
		return view('admin/prices/create');
	}

	// Store new price
	public function store(Request $request)
	{
		$input = $request->all();

		if($input['days'])
		{
			Price::create($input);

			return redirect('/admin/prices');
		}
	}


	// Edit current price
	public function edit($id)
	{
		$price = Price::findOrFail($id);
			
		return view('admin/prices/edit', compact('price'));
	}

	// Update current price
	public function priceupdate($id, Request $request)
	{
		$price = Price::findOrFail($id);

		$price->update($request->all());

		return redirect('/admin/prices');
	}

	// Delete current price
	public function destroy($id)
	{
		$price = Price::findOrFail($id);

		$price->delete();

		return redirect('/admin/prices');
	}

}