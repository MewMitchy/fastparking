<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use View;


class ImageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin/images/index');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function storeHeader(Request $request)
	{
		$input = $request->all();

		if($input['image'])
		{
			$imageName = 'header.jpg';

    		$request->file('image')->move(
        		base_path() . '/public/img/backgrounds/', $imageName
    		);

    		return redirect('admin');
		}
	}

	public function storeFooter(Request $request)
	{
		$input = $request->all();

		if($input['image'])
		{
			$imageName = 'footer.jpg';

    		$request->file('image')->move(
        		base_path() . '/public/img/backgrounds/', $imageName
    		);

    		return redirect('admin');
		}
	}

}