<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Appointment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Price;
use App\Social;

use Redirect;
use View;
use PDF;
use Session;
use Input;
use Validator;
use Mail;
use Storage;
use Hash;
use File;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$prices = Price::orderBy('days', 'DESC')->get();
		$facebook = Social::where('name', 'Facebook')->first();
		$twitter = Social::where('name', 'Twitter')->first();
		$linkedin = Social::where('name', 'LinkedIn')->first();
		$telefoonnummer = Social::where('name', 'Telefoon')->first();
		$adminemail = Social::where('name', 'Email')->first();

		return view('/pages/home/index', compact('prices','facebook','twitter','linkedin','telefoonnummer','adminemail'));
	}

	public function steptwo()
	{
		$prices = Price::orderBy('days', 'DESC')->get();
		$facebook = Social::where('name', 'Facebook')->first();
		$twitter = Social::where('name', 'Twitter')->first();
		$linkedin = Social::where('name', 'LinkedIn')->first();
		$telefoonnummer = Social::where('name', 'Telefoon')->first();
		$adminemail = Social::where('name', 'Email')->first();

		return view('/pages/form/index', compact('prices','facebook','twitter','linkedin','telefoonnummer','adminemail'));
	}

	public function store(Request $request)
	{
		set_error_handler(null);
		set_exception_handler(null);
		$telefoonnummer = Social::where('name', 'Telefoon')->first();
		$adminemail = Social::where('name', 'Email')->first();
		$data = array('phone' => $telefoonnummer->url, 'email' => $adminemail->url);
		$input = $request->all();

        $rules = array (
        	'vliegveld' => 'required',
        	'vertrekdatum' => 'required',
        	'aankomstdatum' => 'required',
        	'vertrektijd' => 'required',
        	'aankomsttijd' => 'required',
        	'aanhef' => 'required',
        	'voorletters' => 'required',
        	'achternaam' => 'required|alpha',
            'telefoon'=>'numeric|min:8',
            'automerk' => 'required',
        	'autokleur' => 'required'
        );

        $validator  = Validator::make ($input, $rules);

        if ($validator -> passes()){
        	Appointment::create($input);
			Session::put('prijs', $input['prijs']);
			Session::put('vliegveld', $input['vliegveld']);
			Session::put('vertrekdatum', $input['vertrekdatum']);
			Session::put('aankomstdatum', $input['aankomstdatum']);
			Session::put('vertrektijd', $input['vertrektijd']);
			Session::put('aankomsttijd', $input['aankomsttijd']);
			Session::put('aanhef', $input['aanhef']);
			Session::put('voorletters', $input['voorletters']);
			Session::put('achternaam', $input['achternaam']);
			Session::put('email', $input['email']);
			Session::put('telefoon', $input['telefoon']);
			Session::put('automerk', $input['automerk']);
			Session::put('autokleur', $input['autokleur']);
			Session::put('autokenteken', $input['autokenteken']);
			Session::put('opmerkingen', $input['opmerkingen']);
			Session::put('mailemail', $data['email']);
			Session::put('mailphone', $data['phone']);

			$vliegveld = Session::get('vliegveld');

			// $hash = $input['achternaam'].rand(1,999999999);
			$dir = 'invoice/invoice'.$input['achternaam'].rand(1,999999999).'.pdf';
			Session::put('directory', $dir);
			$htmlheader = 
			'<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
			<head>
				<link href="css/pdf.css" rel="stylesheet">
			</head>
			<div style="width:20%;float:left;">
			</div>
			<div style="width:80%;float:left;"
				<h1>Invoice Fastparking.nl</h1>
				<h2>'.$input['aanhef'].' '.$input['voorletters'].' '.$input['achternaam'].'</h2>
				<p>Hieronder staat uw informatie vermeldt, mocht deze informatie niet kloppen, neem dan z.s.m contact met ons op.</p>
			</div>
			<table style="padding:5px 20px 5px 0px"class="table">
				<tbody>
					<tr>
						<th>Vliegveld</th>
						<td>'.$input['vliegveld'].'</td>
					</tr>

					<tr>
						<th>Vertrek datum</th>
						<td>'.$input['vertrekdatum'].' '.$input['vertrektijd'].'</td>
					</tr>

					<tr>
						<th>Aankomst datum</th>
						<td>'.$input['aankomstdatum'].' '.$input['aankomsttijd'].'</td>
					</tr>

					<tr>
						<th>Naam</th>
						<td>'.$input['aanhef'].' '.$input['voorletters'].' '.$input['achternaam'].'</td>
					</tr>

					<tr>
						<th>Email</th>
						<td>'.$input['email'].'</td>
					</tr>
					<tr>
						<th>Telefoonnummer</th>
						<td>'.$input['telefoon'].'</td>
					</tr>

					<tr>
						<th>Auto</th>
						<td>'.$input['automerk'].' '.$input['autokleur'].'</td>
					</tr>';

			if($input['autokenteken'] == ""){
				$htmlkenteken = '
					<tr>
						<th>Kenteken</th>
						<td>Niet van toepassing</td>
					</tr>
				';
			}
			else{
				$htmlkenteken = '
					<tr>
						<th>Kenteken</th>
						<td>'.$input['autokenteken'].'</td>
					</tr>
				';
			}
			if($input['opmerkingen'] == ""){
				$htmlopmerkingen = '
					<tr>
						<th>Opmerkingen</th>
						<td>Niet van toepassing</td>
					</tr>
				';
			}
			else{
				$htmlopmerkingen = '
					<tr>
						<th>Opmerkingen</th>
						<td>'.$input['opmerkingen'].'</td>
					</tr>
				';
			}
			$htmlfooter = 
			'</tbody>
			</table>
			<p>Uw totale prijs is <strong>'.$input['prijs'].'</strong></p>

			<h4>Instructies</h4>
			<ol>
				<li>
					Print dit formulier twee maal uit, een voor u, en een voor ons.
				</li>
				<li>
					Zet uw handtekening op beide formulieren.
				</li>
				<li>
					Neem deze formulieren mee op de dag van vertrek.
				</li>
				<li>
					Op de dag van vertrek geeft u een document af bij ons.
				</li>
				<li>
					Bewaar het document goed, dit is uw garantiebewijs!
				</li>
			</ol>

			<strong>Handtekening vereist*</strong>
			<img src="http://fastpark.dev/img/field.png" width="706" height="303"/>';
			
			PDF::loadHTML($htmlheader.$htmlkenteken.$htmlopmerkingen.$htmlfooter)->save($dir)->download();

			// $save = $pdf->PDF::render()->save('invoice.pdf');
			// $download = $pdf->download('invoice.pdf');

            //Send email using Laravel send function
            Mail::send('emails.confirmation', $input, function($message) use ($input, $data, $dir){
				//email 'From' field: Get users email add and name
                $message->from('admin@mitchjj.com' , 'Fastparking');
				//email 'To' field: cahnge this to emails that you want to be notified.                    
				// $message->to('me@gmail.com', 'my name')->cc('me@gmail.com')->subject('contact request');
				$message->to($input['email'], 'Mitchell')->cc($data['email'])->subject('U heeft een nieuwe afspraak gemaakt!')->attach($dir);

            });
            $redirect = redirect('/step-three#appointment');

            // return array($redirect);
            return $redirect;
        }
        else{
			//return contact form with errors
            return Redirect::to('/step-two#appointment')->withErrors($validator);
        }
	}

	public function stepthree()
	{
		$prices = Price::orderBy('days', 'DESC')->get();
		$dir = Session::get('directory');

		$prijs = Session::get('prijs');
		$vliegveld = Session::get('vliegveld');
		$vertrekdatum = Session::get('vertrekdatum');
		$aankomstdatum = Session::get('aankomstdatum');
		$vertrektijd = Session::get('vertrektijd');
		$aankomsttijd = Session::get('aankomsttijd');
		$aanhef = Session::get('aanhef');
		$voorletters = Session::get('voorletters');
		$achternaam = Session::get('achternaam');
		$email = Session::get('email');
		$telefoon = Session::get('telefoon');
		$automerk = Session::get('automerk');
		$autokleur = Session::get('autokleur');
		$autokenteken = Session::get('autokenteken');
		$opmerkingen = Session::get('opmerkingen');
		$mailemail = Session::get('mailemail');
		$mailphone = Session::get('mailphone');

		$facebook = Social::where('name', 'Facebook')->first();
		$twitter = Social::where('name', 'Twitter')->first();
		$linkedin = Social::where('name', 'LinkedIn')->first();
		$telefoonnummer = Social::where('name', 'Telefoon')->first();
		$adminemail = Social::where('name', 'Email')->first();

		File::delete($dir);

		// $data = array('phone' => $telefoonnummer->url, 'email' => $adminemail->url);
		//$data['email'] = $adminemail;

		return view('/pages/confirmation/index', compact('prices',
			'prijs', 
			'vliegveld', 
			'vertrekdatum', 
			'aankomstdatum',
			'vertrektijd',
			'aankomsttijd',
			'aanhef',
			'voorletters',
			'achternaam',
			'email',
			'telefoon',
			'automerk',
			'autokleur',
			'autokenteken',
			'opmerkingen',
			'facebook',
			'twitter',
			'linkedin',
			'mailphone',
			'mailemail',
			'adminemail',
			'telefoonnummer'
			));

	}

}
