<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Social;

use Redirect;
use View;


class SocialController extends Controller {

	public function index()
	{
		$facebook = Social::where('name', 'Facebook')->first();
		$twitter = Social::where('name', 'Twitter')->first();
		$linkedin = Social::where('name', 'LinkedIn')->first();
		$telefoon = Social::where('name', 'Telefoon')->first();
		$email = Social::where('name', 'Email')->first();
			
		return view('admin/social', compact('facebook','twitter','linkedin', 'telefoon','email'));
	}

	public function updatefb(Request $request)
	{
		$facebook = Social::where('name', 'Facebook')->first();

		$facebook->update($request->all());

		return redirect('/admin');
	}

	public function updatetw(Request $request)
	{
		$twitter = Social::where('name', 'Twitter')->first();

		$twitter->update($request->all());

		return redirect('/admin');
	}

	public function updateln(Request $request)
	{
		$linkedin = Social::where('name', 'LinkedIn')->first();

		$linkedin->update($request->all());

		return redirect('/admin');
	}

	public function updatetf(Request $request)
	{
		$telefoon = Social::where('name', 'Telefoon')->first();

		$telefoon->update($request->all());

		return redirect('/admin');
	}

	public function updateem(Request $request)
	{
		$email = Social::where('name', 'Email')->first();

		$email->update($request->all());

		return redirect('/admin');
	}

}