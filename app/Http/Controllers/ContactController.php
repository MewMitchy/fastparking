<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Redirect;
use View;
use Input;
use Validator;
use Mail;


class ContactController extends Controller {

	public function contact(){
 
        //Get all the data and store it inside Store Variable
        $data = Input::all();

        //Validation rules
        $rules = array (
            'name' => 'required|space',
            'phone'=>'numeric|min:8',
            'email' => 'required|email',
            'message' => 'required|min:25'
        );

        //Validate data
        $validator  = Validator::make ($data, $rules);

        //If everything is correct than run passes.
        if ($validator -> passes()){
            //Send email using Laravel send function
            Mail::send('emails.contact', $data, function($message) use ($data){
				//email 'From' field: Get users email add and name
                $message->from('admin@mitchjj.com' , $data['name']);
				//email 'To' field: cahnge this to emails that you want to be notified.                    
				// $message->to('me@gmail.com', 'my name')->cc('me@gmail.com')->subject('contact request');
				$message->to('mitchell@studiostomp.nl', 'Mitchell')->subject('You have a new message!');

            });
            return redirect('/');
        }
        else{
			//return contact form with errors
            return Redirect::to('/')->withErrors($validator);
        }
    }

}