<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('home', 'HomeController@index');
Route::get('/step-two', 'HomeController@steptwo');

Route::get('admin', 'AdminController@index');

// Prices
Route::get('admin/prices', 'AdminController@prices');
Route::get('admin/prices/create', array('as' => 'createprice', 'uses' => 'AdminController@createprice'));
Route::post('admin/prices/', 'AdminController@store');
Route::get('admin/prices/{id}/edit', 'AdminController@edit');
Route::put('admin/prices/{id}', array('as' => 'updateprice', 'uses' => 'AdminController@priceupdate'));
Route::patch('admin/prices/{id}', 'AdminController@priceupdate');
Route::delete('admin/prices/{id}/delete', 'AdminController@destroy');

// Social
Route::get('admin/social', 'SocialController@index');

Route::put('admin/socialfb', 'SocialController@updatefb');
Route::patch('admin/socialfb', 'SocialController@updatefb');

Route::put('admin/socialtw', 'SocialController@updatetw');
Route::patch('admin/socialtw', 'SocialController@updatetw');

Route::put('admin/socialln', 'SocialController@updateln');
Route::patch('admin/socialln', 'SocialController@updateln');

Route::put('admin/socialtf', 'SocialController@updatetf');
Route::patch('admin/socialtf', 'SocialController@updatetf');

Route::put('admin/socialem', 'SocialController@updateem');
Route::patch('admin/socialem', 'SocialController@updateem');

// Images
Route::get('admin/images', 'ImageController@index');
Route::post('admin/images/header', 'ImageController@storeHeader');
Route::post('admin/images/footer', 'ImageController@storeFooter');

// Contact
Route::post('contact', 'ContactController@contact');

// Userinfo
Route::get('admin/password', 'AdminController@password');
Route::put('admin/{id}', 'AdminController@update');
Route::patch('admin/{id}', 'AdminController@update');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/step-two', array('as' => 'createappointment', 'uses' => 'HomeController@steptwo'));
Route::post('/step-two', 'HomeController@store');
Route::get('/step-three', 'HomeController@stepthree');
// Route::get('music/{id}/edit', 'MusicController@edit');
// Route::put('music/{id}', array('as' => 'updatemusic', 'uses' => 'MusicController@update'));
// Route::patch('music/{id}', 'MusicController@update');
// Route::delete('music/{id}/delete', 'MusicController@destroy');