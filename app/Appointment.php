<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model {

	protected $table = 'appointments';

    protected $fillable = [
    	'aanhef',
    	'voorletters',
    	'achternaam',
    	'email',
    	'telefoon',
    	'automerk',
    	'autokleur',
    	'autokenteken',
    	'opmerkingen',
    	'vliegveld',
    	'vertrekdatum',
    	'aankomstdatum',
    	'vertrektijd',
    	'aankomsttijd',
    	'prijs',
    	'dagen'
    ];

}