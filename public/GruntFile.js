module.exports = function(grunt) {

  grunt.initConfig({
    compass: {
      dist: {
        options: {
          config: 'config.rb',
          'output-style': 'compressed'
        }
      },
      dev: {
        options: {
          config: 'config.rb',
          'output-style': 'expanded',
          watch: true
        }
      }
    },
    requirejs: {
      compile: {
        options: {
          baseUrl: "./",
          mainConfigFile: "js/main.js",
          include: "./js/main",
          name: "./bower_components/almond/almond",
          out: "./js/built/main.min.js",
          preserveLicenseComments: true
        }
      }
    },
    clean: {
      css: ['css']
    }
  });

  // Displays the elapsed execution time of grunt tasks
  require('time-grunt')(grunt);

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-requirejs');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-clean');

  // Tasks
  grunt.registerTask('dev', ['clean:css', 'compass:dev']);

  grunt.registerTask('dist', ['clean:css', 'compass:dist', 'requirejs:compile']);


};