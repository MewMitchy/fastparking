$(document).ready(function(){
			$('.input-daterange input').datepicker({
				todayHighlight: true,
				format : "mm/dd/yyyy"
			});
			$('.input-daterange input').each(function() {
			    $(this).datepicker("clearDates");
			});

			// cache
			var result = $(".form-result");
			var totaldays = $("#form-price-for");
			var departure = $("#val-one");
			var arrival = $("#val-two");

			$('html').click(function() {
				var now  = moment($("#val-one").val());
				var then = moment($("#val-two").val());


				var a = moment(now);
				var b = moment(then);
				var difference = b.diff(a, 'days');
				$('.table tbody tr').each(function() {
				    if(difference <= $(this).children('.table-days').text()){
						price = difference * $(this).children('.table-price').text();
					}
				});

				if($("#val-one").val() != "" && $("#val-two").val() != ""){
					if(difference <= 0){
						$("#form-price-for").text("U kunt niet boeken in het verleden");
						price = "";		
					}
					if(difference == 0){
						$("#form-price-for").text("U kunt niet boeken in het verleden");		
						price = "";	
					}
					if(difference == 1){
						$("#form-price-for").text("Prijs voor " + difference + " dag");		
					}
					if(difference > 1){
						$("#form-price-for").text("Prijs voor " + difference + " dagen");	
						result.val('\u20AC' + price + ",-");	
					}
					result.val('\u20AC' + price + ",-");	
				}
				localStorage.setItem("departure", departure.val());
				localStorage.setItem("arrival", arrival.val());
				localStorage.setItem("result", result.val());
				localStorage.setItem("totaldays", difference);
				
			});

			$('.form-date').click(function(){
				var now  = moment($("#val-one").val()).locale("nl").lang("nl").format('LLL');
				var then = moment($("#val-two").val()).locale("nl").lang("nl").format('LLL');

				var a = moment(now);
				var b = moment(then);
				var difference = b.diff(a, 'days');       // 1
				if($("#val-one").val() != "" && $("#val-two").val() != ""){
					result.val(difference + " days");	
				}
				localStorage.setItem("departure", departure.val());
				localStorage.setItem("arrival", arrival.val());
				localStorage.setItem("result", result.val());
				localStorage.setItem("totaldays", difference);
				$("#form-step-one").submit(function( event ) {
					if ( $("#val-one").val() == "" || $("#val-two").val() == "") {
						event.preventDefault();
					}
					if (price == 0) {
						event.preventDefault();
					}
					return;
				});
			});

		});