$(document).ready(function(){
	$('.form-time').timepicker({
		 showMeridian: false
	});

	$(".form-result").val(localStorage.getItem("result"));
	$("#val-one").val(localStorage.getItem("departure"));
	$("#val-two").val(localStorage.getItem("arrival"));
	if(localStorage.getItem("totaldays") == 1){
		$("#form-price-for").text("Prijs voor " + localStorage.getItem("totaldays") + " dag");
	}
	else{
		$("#form-price-for").text("Prijs voor " + localStorage.getItem("totaldays") + " dagen");
	}
	$("#aankomst-one").text("Aankomst tijd Schiphol " + localStorage.getItem("departure"));
	$("#aankomst-two").text("Aankomst tijd Schiphol " + localStorage.getItem("arrival"));


	$(".form-step-two").submit(function( event ) {
		if ( $("#val-one").val() == "" || $("#val-two").val() == "") {
			event.preventDefault();
		}
		if (price == 0) {
			event.preventDefault();
		}
		return;
	});
});