@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Header afbeelding aanpassen</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					{!! Form::open(['url' => 'admin/images/header', 'novalidate' => 'novalidate', 'files' => true]) !!}
						<div class="form-group">
							{!! Form::label('image', 'Header afbeelding') !!}
							{!! Form::file('image', null) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Update afbeelding', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading">Footer afbeelding aanpassen</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					{!! Form::open(['url' => 'admin/images/footer', 'novalidate' => 'novalidate', 'files' => true]) !!}
						<div class="form-group">
							{!! Form::label('image', 'Footer afbeelding') !!}
							{!! Form::file('image', null) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Update afbeelding', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
