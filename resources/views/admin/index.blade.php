@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
				<thead>
					<tr>
						<th>Prijs</th>
						<th>Vertrek</th>
						<th>Aankomst</th>
						<th>Vliegveld</th>
						<th>Naam</th>
						<th>Email</th>
						<th>Telefoon</th>
						<th>Auto</th>
						<th>Kenteken</th>
						<th>Opmerkingen</th>

					</tr>
				</thead>
				<tbody>
					@foreach($appointments as $appointment)
						<tr>
							<td data-th="Prijs">{{$appointment->prijs}}</td>
							<td data-th="Vertrek"><span class="time">{{$appointment->vertrekdatum}}</span> {{$appointment->vertrektijd}}</td>
							<td data-th="Aankomst"><span class="time">{{$appointment->aankomstdatum}}</span> {{$appointment->aankomsttijd}}</td>
							<td data-th="Vliegveld">{{$appointment->vliegveld}}</td>
							<td data-th="Naam">{{$appointment->aanhef}} {{$appointment->voorletters}} {{$appointment->achternaam}}</td>
							<td data-th="Email">{{$appointment->email}}</td>
							<td data-th="Telefoon">{{$appointment->telefoon}}</td>
							<td data-th="Auto">{{$appointment->automerk}} {{$appointment->autokleur}}</td>
							@if($appointment->autokenteken != "")
								<td data-th="Kenteken">{{$appointment->autokenteken}}</td>
							@else
								<td data-th="Kenteken">Geen kenteken gespecificeerd</td>
							@endif

							@if($appointment->opmerkingen != "")
								<td data-th="Opmerkingen">{{$appointment->opmerkingen}}</td>
							@else
								<td data-th="Opmerkingen">Geen opmerkingen</td>
							@endif

						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

<script src="js/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
<script type="text/javascript" src="js/nl.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var time = $(".time").val();
		$('.time').each(function() {
			moment($(".time").text()).format("DD/MM/YYYY");
			$(this).text(moment($(this).text()).format("DD/MM/YYYY"));
			console.log($(this).text());

		});
		moment(time).format("DD/MM/YYYY");
	});
</script>