@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Socialmedia aanpassen</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<span style="color:red">Laat de "URL"/"Telefoonnummer"/"Email" velden leeg, als u deze niet op de website wilt tonen.</span><hr>

					{!! Form::model($facebook, ['method' => 'PATCH', 'action' => ['SocialController@updatefb']]) !!}
						<div class="form-group">
							{!! Form::label('name', 'Naam') !!}
							{!! Form::text('name', null, ['class' => 'form-control', 'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('url', 'Url') !!}
							{!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'U kunt hier de url veranderen']) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Wijzigingen doorvoeren', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}

					<hr>

					{!! Form::model($twitter, ['method' => 'PATCH', 'action' => ['SocialController@updatetw']]) !!}
						<div class="form-group">
							{!! Form::label('name', 'Naam') !!}
							{!! Form::text('name', null, ['class' => 'form-control', 'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('url', 'Url') !!}
							{!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'U kunt hier de url veranderen']) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Wijzigingen doorvoeren', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}

					<hr>

					{!! Form::model($linkedin, ['method' => 'PATCH', 'action' => ['SocialController@updateln']]) !!}
						<div class="form-group">
							{!! Form::label('name', 'Naam') !!}
							{!! Form::text('name', null, ['class' => 'form-control', 'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('url', 'Url') !!}
							{!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'U kunt hier de url veranderen']) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Wijzigingen doorvoeren', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}

					<hr>

					{!! Form::model($telefoon, ['method' => 'PATCH', 'action' => ['SocialController@updatetf']]) !!}
						<div class="form-group">
							{!! Form::label('name', 'Naam') !!}
							{!! Form::text('name', null, ['class' => 'form-control', 'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('url', 'Telefoonnummer') !!}
							{!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'U kunt hier uw telefoonnummer veranderen']) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Wijzigingen doorvoeren', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}

					{!! Form::model($email, ['method' => 'PATCH', 'action' => ['SocialController@updateem']]) !!}
						<div class="form-group">
							{!! Form::label('name', 'Naam') !!}
							{!! Form::text('name', null, ['class' => 'form-control', 'readonly']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('url', 'Email') !!}
							{!! Form::text('url', null, ['class' => 'form-control', 'placeholder' => 'U kunt hier uw email veranderen']) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Wijzigingen doorvoeren', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
