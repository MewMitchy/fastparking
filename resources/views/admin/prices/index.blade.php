@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<table class="table">
				<thead>
					<tr>
						<th>Dagen</th>
						<th>Prijs</th>
						<th>Aanpassen</th>
						<th>Verwijderen</th>
					</tr>
				</thead>
				<tbody>
					@foreach($prices as $price)
						<tr>
							<td  data-th="Dagen">{{$price->days}}</td>
							<td  data-th="Prijs">{{$price->price}}</td>
							<td  data-th="Aanpassen"><a href="/admin/prices/{{$price->id}}/edit">Aanpassen</a></td>
							<td  data=th="Verwijderen">
							{!! Form::open(['action' => ['AdminController@destroy', $price->id], 'method' => 'delete']) !!}
				  				{!! Form::submit('Verwijderen', ['class'=>'form-delete']) !!}
							{!! Form::close() !!}
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="form-group">
				<a href="/admin/prices/create"><button class="form-control form-submit form-price-button">Maak een nieuwe aan</button></a>
			</div>
		</div>
	</div>
</div>
@endsection
