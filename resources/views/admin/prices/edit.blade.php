@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Prijs aanpassen</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<?php echo Form::model($price, ['route' => ['updateprice', $price->id], 'method' => 'patch']);
						echo '<div class="form-group">';
							echo Form::label('days', 'Dagen');
							echo Form::text('days', null, ['class' => 'form-control', 'placeholder' => 'Vul in voor hoeveel dagen de prijs geldt']);
						echo '</div>';

						echo '<div class="form-group">';
							echo Form::label('price', 'Prijs');
							echo Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Vul in voor hoeveel de prijs is']);
						echo '</div>';


						echo '<div class="form-group">';
							echo Form::submit('Update prijs', ['class' => 'btn btn-primary form-control']);
						echo '</div>';
					?>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
