@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Prijs aanmaken</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					{!! Form::open(['url' => 'admin/prices']) !!}
						<div class="form-group">
							{!! Form::label('days', 'Dagen') !!}
							{!! Form::text('days', null, ['class' => 'form-control', 'placeholder' => 'Vul in voor hoeveel dagen de prijs geldt']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('price', 'Prijs') !!}
							{!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Vul in voor hoeveel de prijs is']) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Voeg toe', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
