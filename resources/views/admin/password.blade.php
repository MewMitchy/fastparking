@extends('app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">Account aanpassen</div>
				<div class="panel-body">
					@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
					@endif

					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					{!! Form::model($user, ['method' => 'PATCH', 'action' => ['AdminController@update']]) !!}
						<div class="form-group">
							{!! Form::label('name', 'Naam') !!}
							{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'U kunt hier uw naam veranderen']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('email', 'Email') !!}
							{!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'U kunt hier uw email veranderen']) !!}
						</div>
						<div class="form-group">
							{!! Form::label('password', 'Wachtwoord') !!}
							{!!	Form::password('password',array('minlength'=>6,'maxlength'=>25,'id'=>'password','class'=>'form-control','placeholder'=>'U kunt hier uw wachtwoord veranderen','title'=>'password')) !!}
						</div>

						<div class="form-group">
							{!! Form::submit('Wijzigingen doorvoeren', ['class' => 'btn btn-primary form-control']) !!}
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
