{{-- <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Neem contact met ons op!</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Uw naam *" id="name" required data-validation-required-message="Vul hier uw naam in.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Uw email *" id="email" required data-validation-required-message="Vul hier uw email in.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Uw telefoonnummer *" id="phone" required data-validation-required-message="Vul hier uw telefoonnummer in.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Uw bericht *" id="message" required data-validation-required-message="Vul hier uw bericht in."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Stuur bericht</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section> --}}


<!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Neem contact met ons op!</h2>
                    <h3 class="section-subheading text-muted">Vul het onderstaande formulier in voor vragen!</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    {!! Form:: open(array('url' => 'contact', 'class' => 'form-step-two', 'name' => 'sentMessage', 'novalidate')) !!} <!--contact_request is a router from Route class-->
						<div class="row">
					 
						    <ul class="errors">
						        @foreach($errors->all('<li>:message</li>') as $message)
						        {{ $message }}
						        @endforeach
						    </ul>

						    <div class="col-md-6">
					            <div class="form-group">
					                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Uw naam *', 'required', 'data-validation-required-message' => 'Vul hier uw naam in.']) !!}
					                <p class="help-block text-danger"></p>
					            </div>
					            <div class="form-group">
					                {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Uw email *', 'required', 'data-validation-required-message' => 'Vul hier uw email in.']) !!}
					                <p class="help-block text-danger"></p>
					            </div>
					            <div class="form-group">
					                {!! Form::input('number', 'phone', null, ['class' => 'form-control', 'placeholder' => 'Uw telefoonnummer *', 'required', 'data-validation-required-message' => 'Vul hier uw telefoonnummer in.']) !!}
					                <p class="help-block text-danger"></p>
					            </div>
					        </div>

					        <div class="col-md-6">
					            <div class="form-group">
					                {!! Form::textarea('message', null, ['class' => 'form-control', 'placeholder' => 'Uw bericht *', 'required', 'data-validation-required-message' => 'Vul hier uw bericht in.']) !!}
					                <p class="help-block text-danger"></p>
					            </div>
					        </div>

					        <div class="clearfix"></div>
					        <div class="col-lg-12 text-center">
					            <div id="success"></div>
					            {!! Form::submit('Stuur bericht', array('class' => 'btn btn-xl')) !!}
					        </div>

					    </div>
					 
					{!! Form:: close() !!}
                </div>
            </div>
        </div>
    </section>