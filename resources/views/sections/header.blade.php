<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Fastparking | Schiphol carparking</title>
    <meta name="description" content="Parkeren op Schiphol, laat dat maar over aan Fastparking">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<meta name="apple-mobile-web-app-title" content="Fastparking">

	<!-- Open graph metadata -->
	<meta property="og:type" content="website" />
	<meta property="og:locale" content="nl_NL" />
	<meta property="og:title" content="Fastparking - Schiphol valet parking"/>
	<meta property="og:url" content="http://www.fastparking.com/"/>
	<meta property="og:image" content=""/>
	<meta property="og:site_name" content="Fastparking"/>
	<meta property="og:description" content="Parkeren op Schiphol, laat dat maar over aan Fastparking"/>
	<link rel="canonical" href="http://fastparking.com/" />

    <!-- Custom CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="css/style.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Fast<span>parking</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    {{-- <li>
                        <a class="page-scroll" href="#services">Services</a>
                    </li> --}}
                    <li>
                        <a class="page-scroll" href="#appointment">Boek nu</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#pricing">Prijzen</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div class="intro-lead-in">Schiphol carparking</div>
                <div class="intro-heading">Fast<span>parking</span></div>
                <div class="col-md-6 col-md-offset-3 intro-buttons">
                    <a href="#appointment" class="page-scroll btn btn-xl col-md-12 intro-btn-book">Boek nu!</a>
                </div>
                <div class="col-md-6 col-md-offset-3 intro-buttons bottom">
                    <a href="" class="page-scroll btn btn-xl col-md-6 col-xs-6 intro-btn-call">Bel nu!</a>
                    <a href="#pricing" class="page-scroll btn btn-xl col-md-6 col-xs-6 intro-btn-price">Tarieven</a>
                </div>
            </div>
        </div>
    </header>