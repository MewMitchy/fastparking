
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">
                        Copyright &copy; Fastparking 2015
                    </span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        @if($facebook->url != '')
                            <li><a href="{{$facebook->url}}"><i class="fa fa-facebook"></i></a></li>
                        @endif

                        @if($twitter->url != '')
                            <li><a href="{{$twitter->url}}"><i class="fa fa-twitter"></i></a></li>
                        @endif

                        @if($linkedin->url != '')
                            <li><a href="{{$linkedin->url}}"><i class="fa fa-linkedin"></i></a></li>
                        @endif
                    </ul>
                </div>
                <div class="col-md-4">
                     <span class="copyright">
                    @if($telefoonnummer->url != '')
                        {{$telefoonnummer->url}} |
                    @endif
                     @if($adminemail->url != '')
                        {{$adminemail->url}}
                    @endif
                    </span>
                </div>
            </div>
        </div>
    </footer>