@include('../../sections/header')

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2>
                    <h3 class="section-subheading text-muted">Wat wij voor jou kunnen doen</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-car fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Fast Parking</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Online</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Veilig</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Afspraak Section -->
    <section id="appointment" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Vul hier uw persoonlijke informatie in!</h2>
                    <h3 class="section-subheading text-muted">Zorg ervoor dat alle informatie klopt!</h3>
                </div>
            </div>
            <div class="row">
            	<div class="col-lg-12">

					{!! Form::open(['url' => '/step-two', 'class' => "form-step-two"]) !!}
						<div class="form-group">
							<hr>
							<h4>Data gegevens</h4>
						</div>

						<div class="form-group">
							{!! Form::label('vliegveld', 'Vliegveld') !!}
							{!! Form::select('vliegveld', array('Schiphol' => 'Schiphol'), null, ['id' => 'form-selector','class' => 'form-control', 'readonly']) !!}
						</div>

						<div class="input-group form-group input-daterange">
							{!! Form::text('vertrekdatum', null, ['id' => 'val-one','class' => 'form-control form-date col-md-6 col-xs-12', 'placeholder' => 'Vertrek datum', 'readonly']) !!}
						    <span class="input-group-addon">tot</span>
						    {!! Form::text('aankomstdatum', null, ['id' => 'val-two','class' => 'form-control form-date col-md-6 col-xs-12', 'placeholder' => 'Aankomst datum', 'readonly']) !!}
						</div>

						<div class="form-group">
							<label id="form-price-for">Prijs voor</label>
							{!! Form::label(null, null, ['class' => 'form-price-for', 'readonly']) !!}
							{!! Form::text('prijs', null, ['class' => 'form-control form-result', 'readonly']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('vertrektijd', 'Aankomst tijd Schiphol', ['id' => 'aankomst-one']) !!}
							{!! Form::text('vertrektijd', null, ['class' => 'form-control form-time', 'placeholder' => 'Vul hier in hoelaat u op Schiphol bent']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('aankomsttijd', 'Aankomst tijd Schiphol', ['id' => 'aankomst-two']) !!}
							{!! Form::text('aankomsttijd', null, ['class' => 'form-control form-time', 'placeholder' => 'Vul hier in hoelaat u op Schiphol bent']) !!}
						</div>

						<div class="form-group">
							<hr>
							<h4>Persoonlijke gegevens</h4>
						</div>

						<div class="form-group">
							{!! Form::label('aanhef', 'Aanhef') !!}
							{!! Form::select('aanhef', array('Dhr.' => 'Dhr.', 'Mevr.' => 'Mevr.'), null, ['id' => 'form-selector','class' => 'form-control']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('voorletters', 'Uw voorletters') !!}
							{!! Form::text('voorletters', null, ['class' => 'form-control', 'placeholder' => 'Vul hier uw voorletters in']) !!}
						</div>


						<div class="form-group">
							{!! Form::label('achternaam', 'Uw achternaam') !!}
							{!! Form::text('achternaam', null, ['class' => 'form-control', 'placeholder' => 'Vul hier uw achternaam in']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('email', 'Uw email') !!}
							{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Vul hier uw email in']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('telefoon', 'Uw telefoonnummer') !!}
							{!! Form::input('number', 'telefoon', null, ['class' => 'form-control', 'placeholder' => 'Vul hier uw telefoonnummer in']) !!}
						</div>

						<div class="form-group">
							<hr>
							<h4>Auto gegevens</h4>
						</div>
						
						<div class="form-group">
							{!! Form::label('automerk', 'Auto merk') !!}
							{!! Form::text('automerk', null, ['class' => 'form-control', 'placeholder' => 'Vul hier het merk van uw auto in']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('autokleur', 'Auto kleur') !!}
							{!! Form::text('autokleur', null, ['class' => 'form-control', 'placeholder' => 'Vul hier de kleur van uw auto in']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('autokenteken', 'Auto kenteken') !!}
							{!! Form::text('autokenteken', null, ['class' => 'form-control', 'placeholder' => 'Vul hier eventueel uw kenteken in']) !!}
						</div>

						<div class="form-group">
							{!! Form::label('opmerkingen', 'Opmerkingen') !!}
							{!! Form::textarea('opmerkingen', null, ['class' => 'form-control form-date', 'placeholder' => 'Eventuele opmerkingen']) !!}
						</div>
						
						<div class="form-group">
							{!! Form::button('Go back ', ['class' => 'form-control form-return', 'onclick' => 'location.href="/";return true;']) !!}
							{!! Form::submit('Submit ', ['class' => 'form-control form-submit']) !!}
						</div>

						

					{!! Form::close() !!}
				</div>
            </div>
        </div>
    </section>

    <!-- Pricing Section -->
    <section id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tarieven</h2>
                    <h3 class="section-subheading text-muted">Alle informatie wat betreft prijzen</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <table class="table">
						<thead>
							<tr>
								<th class="text-center">Dagen</th>
								<th class="text-center">Prijs/dag</th>
							</tr>
						</thead>
						<tbody>
							@foreach($prices as $price)
								<tr>
									<td class="table-days">{{$price->days}}</td>
									<td class="table-price">{{$price->price}}</td>
								</tr>
							@endforeach
						</tbody>
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </section>

@include('../../sections/contact')
@include('../../sections/footer')
@include('/pages/form/script')