
    <!-- jQuery -->
    <script src="js/jquery/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="js/bootstrap/datepicker.js"></script>
	<script type="text/javascript" src="js/moment/moment.js"></script>
	<script type="text/javascript" src="js/moment/nl.js"></script>
	<script type="text/javascript" src="js/bootstrap/bootstrap.js"></script>
	<script type="text/javascript" src="js/bootstrap/readable-range.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/logo.js"></script>
	

    <!-- Plugin JavaScript -->
    <script src="js/jquery/easing.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>

    <!-- Contact Form JavaScript -->
    
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/agency.js"></script>

</body>

</html>