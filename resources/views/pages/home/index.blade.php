@include('../../sections/header')

    <!-- Services Section -->
    {{-- <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2>
                    <h3 class="section-subheading text-muted">Wat wij voor jou kunnen doen</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-car fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Fast Parking</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Online</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Veilig</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
            </div>
        </div>
    </section> --}}

    <!-- Afspraak Section -->
    <section id="appointment" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Maak nu een afspraak!</h2>
                    <h3 class="section-subheading text-muted">Zodat u zorgeloos op vakantie kunt gaan!</h3>
                </div>
            </div>
            <div class="row">
            	<div class="col-lg-12">
	                <form action="/step-two#appointment" id="form-step-one">
						<div class="form-group">
							<label for="form-selector">Vliegveld</label>
							<select class="form-control" id="form-selector">
								<option>Schiphol</option>
							</select>
						</div>
						<div class="input-group form-group input-daterange">
						    <input type="text" class="form-control form-date col-md-6 col-xs-12" id="val-one" value="" placeholder="Vertrek datum">
						    <span class="input-group-addon">tot</span>
						    <input type="text" class="form-control form-date col-md-6 col-xs-12" id="val-two" value="" placeholder="Aankomst datum">
						</div>
						<div class="form-group">
							<label id="form-price-for">Prijs voor</label>
							<input type="text" class="form-control form-result" readonly>
						</div>
						<input type="submit" class="form-control form-submit">
					</form>
				</div>
            </div>
        </div>
    </section>

    <!-- Pricing Section -->
    <section id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tarieven</h2>
                    <h3 class="section-subheading text-muted">Alle informatie wat betreft prijzen</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <table class="table">
						<thead>
							<tr>
								<th class="text-center">Dagen</th>
								<th class="text-center">Prijs/dag</th>
							</tr>
						</thead>
						<tbody>
							@foreach($prices as $price)
								<tr>
									<td class="table-days">{{$price->days}}</td>
									<td class="table-price">{{$price->price}}</td>
								</tr>
							@endforeach
						</tbody>
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </section>

@include('../../sections/contact')
@include('../../sections/footer')
@include('/pages/home/script')