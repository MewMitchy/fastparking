@include('../../sections/header')

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Services</h2>
                    <h3 class="section-subheading text-muted">Wat wij voor jou kunnen doen</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-car fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Fast Parking</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Online</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-4x">
                        <i class="fa fa-circle fa-stack-2x text-primary"></i>
                        <i class="fa fa-lock fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4 class="service-heading">Veilig</h4>
                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
                </div>
            </div>
        </div>
    </section>

    <!-- Afspraak Section -->
    <section id="appointment" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Uw boekingsinformatie</h2>
                    <h3 class="section-subheading text-muted">Deze informatie krijgt u ook toegemaild!</h3>
                </div>
            </div>
            <div class="row">
            	<div class="col-lg-12">
	                <table class="table">
	                	<thead>
	                		<tr>
	                			<th></th>
	                			<th>Uw informatie</th>
	                		</tr>
	                	</thead>
						<tbody>
							<tr>
								<th>Vliegveld</th>
								<td>{{$vliegveld}}</td>
							</tr>

							<tr>
								<th>Vertrek datum</th>
								<td><span class="time">{{$vertrekdatum}}</span> {{$vertrektijd}}</td>
							</tr>

							<tr>
								<th>Aankomst datum</th>
								<td><span class="time">{{$aankomstdatum}}</span> {{$aankomsttijd}}</td>
							</tr>

							<tr>
								<th>Naam</th>
								<td>{{$aanhef}} {{$voorletters}} {{$achternaam}}</td>
							</tr>

							<tr>
								<th>Email</th>
								<td>{{$email}}</td>
							</tr>
							<tr>
								<th>Telefoonnummer</th>
								<td>{{$telefoon}}</td>
							</tr>

							<tr>
								<th>Auto</th>
								<td>{{$automerk}} {{$autokleur}}</td>
							</tr>

							<tr>
								<th>Kenteken</th>
								@if($autokenteken == '')
									<td>Niet van toepassing</td>
								@else
									<td>{{$autokenteken}}</td>
								@endif
							</tr>

							<tr>
								<th>Opmerkingen</th>
								@if($opmerkingen == '')
									<td>Niet van toepassing</td>
								@else
									<td>{{$opmerkingen}}</td>
								@endif
							</tr>

						</tbody>
					</table>
					<h3>Uw totale prijs is <span class="total-price">{{$prijs}}</span></h3>
					<h5>Uw afspraak is gemaakt, en u heeft hierover een email ontvangen met verdere instructies.</h5>
					<p>Als er iets fout gegaan is, neem dan z.s.m contact met ons op!</p>
				</div>
            </div>
        </div>
    </section>

    <!-- Pricing Section -->
    <section id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Tarieven</h2>
                    <h3 class="section-subheading text-muted">Alle informatie wat betreft prijzen</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <table class="table">
						<thead>
							<tr>
								<th class="text-center">Dagen</th>
								<th class="text-center">Prijs/dag</th>
							</tr>
						</thead>
						<tbody>
							@foreach($prices as $price)
								<tr>
									<td class="table-days">{{$price->days}}</td>
									<td class="table-price">{{$price->price}}</td>
								</tr>
							@endforeach
						</tbody>
						</tbody>
					</table>
                </div>
            </div>
        </div>
    </section>

@include('../../sections/contact')
@include('../../sections/footer')
@include('/pages/home/script')