<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<p>94921491</p>
{{$vliegveld}}
{{-- <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<p>iuahsf</p>
<head>
	<link href="css/pdf.css" rel="stylesheet">
</head>
<div style="width:20%;float:left;">
	<img src="http://fastpark.dev/img/header-bg.jpg" width="80" height="40"/>
</div>
<div style="width:80%;float:left;"
	<h1>Invoice Fastparking.nl</h1>
	<p>Hieronder staat uw informatie vermeldt, mocht deze informatie niet kloppen, neem dan z.s.m contact met ons op.</p>
</div>
<table style='padding:5px 20px 5px 0px'class="table">
	<tbody>
		<tr>
			<th>Vliegveld</th>
			<td>{{$vliegveld}}</td>
		</tr>

		<tr>
			<th>Vertrek datum</th>
			<td>{{$vertrekdatum}} {{$vertrektijd}}</td>
		</tr>

		<tr>
			<th>Aankomst datum</th>
			<td>{{$aankomstdatum}} {{$aankomsttijd}}</td>
		</tr>

		<tr>
			<th>Naam</th>
			<td>{{$aanhef}} {{$voorletters}} {{$achternaam}}</td>
		</tr>

		<tr>
			<th>Email</th>
			<td>{{$email}}</td>
		</tr>
		<tr>
			<th>Telefoonnummer</th>
			<td>{{$telefoon}}</td>
		</tr>

		<tr>
			<th>Auto</th>
			<td>{{$automerk}} {{$autokleur}}</td>
		</tr>

		<tr>
			<th>Kenteken</th>
			@if($autokenteken == '')
				<td>Niet van toepassing</td>
			@else
				<td>{{$autokenteken}}</td>
			@endif
		</tr>

		<tr>
			<th>Opmerkingen</th>
			@if($opmerkingen == '')
				<td>Niet van toepassing</td>
			@else
				<td>{{$opmerkingen}}</td>
			@endif
		</tr>

	</tbody>
</table>
<p>Uw totale prijs is <strong>{{$prijs}}</strong></p>

<h4>Instructies</h4>
<ol>
	<li>
		Print dit formulier twee maal uit, een voor u, en een voor ons.
	</li>
	<li>
		Zet uw handtekening op beide formulieren.
	</li>
	<li>
		Neem deze formulieren mee op de dag van vertrek.
	</li>
	<li>
		Op de dag van vertrek geeft u een document af bij ons.
	</li>
	<li>
		Bewaar het document goed, dit is uw garantiebewijs!
	</li>
</ol>

<strong>Handtekening vereist*</strong>
<div style="float:left; width:200px; height:200px; border:1px solid black;">
<div style="float:right; width:400px; height:200px; border:1px solid black;">
<img src="http://fastpark.dev/img/header-bg.jpg" width="100%"/> --}}